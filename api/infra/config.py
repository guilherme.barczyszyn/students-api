import os


def get_mongodb_host():
    return os.getenv('MONGODB_HOST', '127.0.0.1')


def get_mongodb_user():
    return os.getenv('MONGODB_USER', 'mongo')


def get_mongodb_password():
    return os.getenv('MONGODB_PASSWORD', 'mongo')


def get_mongodb_port():
    return os.getenv('MONGODB_PORT', '27017')
