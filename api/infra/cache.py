from flask_caching import Cache

# Estou usando o tipo simple para simplificar. Mas não seria o ideal pois não é threadsafe.
# Se fosse uma aplicação que iria para um ambiente de produção poderiamos pensar em utilizar o Redis ou outro tipo.
cache = Cache(config={'CACHE_TYPE': 'simple', 'CACHE_DEFAULT_TIMEOUT': 300, 'CACHE_THRESHOLD': 10})

