from pymongo import MongoClient

from api.infra.config import get_mongodb_user, get_mongodb_password, get_mongodb_host, get_mongodb_port


class MongoDbRepository:
    def insert(self, param):
        db = self.get_db()

        db.item.insert(param)

    def find_one(self, param):
        db = self.get_db()

        return db.item.find_one(param)

    def find(self, param):
        db = self.get_db()

        return db.item.find(param)

    def find_sorted(self, param, sort, direction):
        db = self.get_db()

        return db.item.find(param).sort([(sort, direction)])

    def distinct(self, field, param):
        db = self.get_db()

        return db.item.distinct(field, param)

    def count(self, param):
        db = self.get_db()

        return db.item.count(param)

    def delete_one(self, param):
        db = self.get_db()

        db.item.delete_one(param)

    def get_db(self):
        client = MongoClient('mongodb://%s:%s@%s:%s' % (get_mongodb_user(), get_mongodb_password(),
                                                        get_mongodb_host(), get_mongodb_port()))

        return client.students_api
