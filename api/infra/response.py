class Response:
    def __init__(self, success, message, data):
        self._success = success
        self._message = message
        self._data = data

    @property
    def successful(self):
        return self._success

    def get_message(self):
        return self._message

    def get_data(self):
        return self._data
