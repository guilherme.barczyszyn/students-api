import json

from flask import Blueprint

from api.domain.course.course_service import CourseService

bp = Blueprint('course', __name__)


@bp.route('/course/<campus>', methods=['GET'])
def list_by_campus(campus):
    service = CourseService()

    response = service.list_courses_by_campus(campus)

    if response.successful:
        courses = response.get_data()

        courses_json = [{"nome": c.nome} for c in courses]

        return json.dumps(courses_json, ensure_ascii=False).encode('utf8'), 200
    else:
        return json.dumps({"Internal server error:": response.get_message()}), 500
