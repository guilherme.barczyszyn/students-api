from api.domain.course.course_repository import CourseRepository
from api.infra.response import Response


class CourseService:
    def __init__(self, repository=None):
        self._repository = CourseRepository() if repository is None else repository

    def list_courses_by_campus(self, campus):
        if campus is None:
            return Response(False, "Campus parameter is required but is missing.", None)

        try:
            courses = self._repository.list_courses_by_campus(campus)
            return Response(True, "", courses)
        except Exception as e:
            return Response(False, "Error when listing courses: " + str(e), None)
