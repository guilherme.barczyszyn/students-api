from api.domain.course.course_model import Course
from api.infra.mongodb_repository import MongoDbRepository


class CourseRepository(MongoDbRepository):
    def list_courses_by_campus(self, campus):
        courses_dict = self.distinct("curso", {"campus": campus})

        courses = []
        for course_dict in courses_dict:
            course = Course({"nome": course_dict})
            courses.append(course)

        return courses
