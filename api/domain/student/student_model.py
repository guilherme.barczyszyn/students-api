class Student:
    def __init__(self, obj_dict=None):
        if obj_dict is not None:
            self._nome = obj_dict["nome"]
            self._idade_ate_31_12_2016 = obj_dict["idade_ate_31_12_2016"]
            self._ra = obj_dict["ra"]
            self._campus = obj_dict["campus"]
            self._municipio = obj_dict["municipio"]
            self._curso = obj_dict["curso"]
            self._modalidade = obj_dict["modalidade"]
            self._nivel_do_curso = obj_dict["nivel_do_curso"]
            self._data_inicio = obj_dict["data_inicio"]

    @property
    def nome(self):
        return self._nome

    @property
    def idade_ate_31_12_2016(self):
        return self._idade_ate_31_12_2016

    @property
    def ra(self):
        return self._ra

    @property
    def campus(self):
        return self._campus

    @property
    def municipio(self):
        return self._municipio

    @property
    def curso(self):
        return self._curso

    @property
    def modalidade(self):
        return self._modalidade

    @property
    def nivel_do_curso(self):
        return self._nivel_do_curso

    @property
    def data_inicio(self):
        return self._data_inicio

    def dict(self):
        return {
            "nome": self._nome,
            "idade_ate_31_12_2016": self._idade_ate_31_12_2016,
            "ra": self._ra,
            "campus": self._campus,
            "municipio": self._municipio,
            "curso": self._curso,
            "modalidade": self._modalidade,
            "nivel_do_curso": self._nivel_do_curso,
            "data_inicio": self._data_inicio
        }
