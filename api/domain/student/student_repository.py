from api.domain.student.student_model import Student
from api.infra.cache import cache
from api.infra.mongodb_repository import MongoDbRepository


class StudentRepository(MongoDbRepository):

    def find_by_ra(self, ra):
        obj_dict = self.find_one({"ra": ra})

        return Student(obj_dict=obj_dict) if obj_dict is not None else None

    def count_registered_between_dates(self, campus, data_inicio, data_fim):
        total = self.count({
            "campus": campus,
            "data_inicio": {
                "$gte": data_inicio,
                "$lt": data_fim
            }
        })

        return total

    def insert_student(self, student):
        self.insert(student.dict())

    def delete_student(self, ra, campus):
        self.delete_one({"ra": ra, "campus": campus})
