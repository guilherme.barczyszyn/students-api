import json

from flask import Blueprint, request

from api.domain.student.student_service import StudentService
from api.infra.cache import cache

bp = Blueprint('student', __name__)


@bp.route('/student/<int:ra>', methods=['GET'])
@cache.cached()
def find(ra):
    service = StudentService()

    response = service.find(int(ra))

    if response.successful and response.get_data() is not None:
        return json.dumps(response.get_data().dict(), ensure_ascii=False).encode('utf8'), 200
    elif response.successful and response.get_data() is None:
        return json.dumps({"message": "Resource not found"}), 204
    else:
        return json.dumps({"Internal server error:": response.get_message()}), 500


@bp.route('/student/count', methods=['GET'])
def count():
    service = StudentService()

    args = request.args

    campus = args["campus"]
    data_inicio = args["data_inicio"]
    data_fim = args["data_fim"]

    response = service.count_registered_between_dates(campus, data_inicio, data_fim)

    if response.successful:
        return json.dumps({"total": response.get_data()}), 200
    else:
        return json.dumps({"message": "Internal server error" + response.get_message()}), 500


@bp.route('/student/new', methods=['PUT'])
def insert():
    service = StudentService()

    nome = request.json['nome']
    idade_ate_31_12_2016 = request.json['idade_ate_31_12_2016']
    ra = request.json['ra']
    campus = request.json['campus']
    municipio = request.json['municipio']
    curso = request.json['curso']
    modalidade = request.json['modalidade']
    nivel_do_curso = request.json['nivel_do_curso']
    data_inicio = request.json['data_inicio']

    response = service.insert_student(nome, idade_ate_31_12_2016, ra, campus, municipio, curso,
                                      modalidade, nivel_do_curso, data_inicio)

    if response.successful:
        return json.dumps({"message": "OK"}), 200
    else:
        return json.dumps({"message": "Internal server error" + response.get_message()}), 500


@bp.route('/student/delete/<int:ra>', methods=['DELETE'])
def delete(ra):
    service = StudentService()

    args = request.args

    campus = args["campus"]

    response = service.delete_student(int(ra), campus)

    if response.successful:
        return json.dumps({"message": "OK"}), 200
    else:
        return json.dumps({"message": "Internal server error" + response.get_message()}), 500
