from api.domain.student.student_model import Student
from api.domain.student.student_repository import StudentRepository
from api.infra.cache import cache
from api.infra.response import Response


class StudentService:

    def __init__(self, repository=None):
        self._repository = StudentRepository() if repository is None else repository

    def find(self, ra):
        if ra is None:
            return Response(False, "RA parameter is required but is missing.", None)

        try:
            student = self._repository.find_by_ra(ra)
            return Response(True, "", student)
        except Exception as e:
            return Response(False, "Error when finding student: " + str(e), None)

    def count_registered_between_dates(self, campus, data_inicio, data_fim):
        if campus is None or data_inicio is None or data_fim is None:
            return Response(False, "One or more required parameter is missing.", None)

        try:
            count = self._repository.count_registered_between_dates(campus, data_inicio, data_fim)
            return Response(True, "", count)
        except Exception as e:
            return Response(False, "Error when querying database: " + str(e), None)

    def insert_student(self, nome, idade_ate_31_12_2016, ra, campus, municipio, curso, modalidade,
                       nivel_do_curso, data_inicio, cache_student=True):
        if nome is None or idade_ate_31_12_2016 is None or ra is None or campus is None or municipio is None \
                or curso is None or modalidade is None or nivel_do_curso is None or data_inicio is None:
            return Response(False, "One or more required parameter is missing.", None)

        try:
            student = Student({"nome": nome, "idade_ate_31_12_2016": idade_ate_31_12_2016, "ra": ra, "campus": campus,
                               "municipio": municipio, "curso": curso, "modalidade": modalidade, "nivel_do_curso": nivel_do_curso,
                               "data_inicio": data_inicio})
            self._repository.insert_student(student)

            if cache_student:
                cache.set('view//student/{}'.format(ra), student.dict())

            return Response(True, "", None)
        except Exception as e:
            return Response(False, "Error when inserting into database: " + str(e), None)

    def delete_student(self, ra, campus):
        if ra is None or campus is None:
            return Response(False, "One or more required parameter is missing.", None)

        try:
            self._repository.delete_student(ra, campus)
            return Response(True, "", None)
        except Exception as e:
            return Response(False, "Error when deleting student: " + str(e), None)