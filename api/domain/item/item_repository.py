import pymongo

from api.domain.item.item_model import Item
from api.infra.mongodb_repository import MongoDbRepository


class ItemRepository(MongoDbRepository):
    def list_by_modality_between_dates(self, modalidade, data_inicio, data_fim):
        items_dict = self.find_sorted({
            "modalidade": modalidade,
            "data_inicio": {
                "$gte": data_inicio,
                "$lt": data_fim
            }}, "data_inicio", pymongo.ASCENDING)

        items = []

        for item_dict in items_dict:
            item = Item(item_dict)

            items.append(item)

        return items

