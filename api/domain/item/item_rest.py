import json

from flask import Blueprint, request

from api.domain.item.item_service import ItemService

bp = Blueprint('item', __name__)


class ItemRest:

    @bp.route('/item/<modalidade>', methods=['GET'])
    def list_items(modalidade):
        service = ItemService()

        args = request.args

        data_inicio = args["data_inicio"]
        data_fim = args["data_fim"]

        response = service.list_by_modality_between_dates(modalidade, data_inicio, data_fim)

        if response.successful:
            items = response.get_data()

            items_json = [i.dict() for i in items]

            return json.dumps(items_json, ensure_ascii=False).encode('utf8'), 200
        else:
            return json.dumps({"Internal server error:": response.get_message()}), 500
