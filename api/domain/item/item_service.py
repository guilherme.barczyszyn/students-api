from api.domain.item.item_repository import ItemRepository
from api.infra.response import Response


class ItemService:
    def __init__(self, repository=None):
        self._repository = ItemRepository() if repository is None else repository

    def list_by_modality_between_dates(self, modalidade, data_inicio, data_fim):
        if modalidade is None or data_inicio is None or data_fim is None:
            return Response(False, "One or more required parameter is missing.", None)

        try:
            items = self._repository.list_by_modality_between_dates(modalidade, data_inicio, data_fim)
            return Response(True, "", items)
        except Exception as e:
            return Response(False, "Error when listing items: " + str(e), None)
