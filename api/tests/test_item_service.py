from api.domain.item.item_service import ItemService


def test_list_by_modality_between_dates(item_repository, modalidade, data_inicial, data_fim):
    service = ItemService(item_repository)

    response = service.list_by_modality_between_dates(modalidade, data_inicial, data_fim)

    assert response.successful


def test_list_by_modality_between_dates_should_fail(item_repository, modalidade, data_inicial):
    service = ItemService(item_repository)

    response = service.list_by_modality_between_dates(modalidade, data_inicial, None)

    assert response.successful is False
