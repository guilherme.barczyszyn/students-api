from api.domain.course.course_service import CourseService


def test_list_courses_by_campus(course_repository, campus):
    service = CourseService(course_repository)

    response = service.list_courses_by_campus(campus)

    assert response.successful


def test_list_courses_by_campus_should_fail(course_repository):
    service = CourseService(course_repository)

    response = service.list_courses_by_campus(None)

    assert response.successful is False