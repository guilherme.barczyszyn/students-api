from unittest.mock import Mock

import pytest

from api.domain.course.course_repository import CourseRepository
from api.domain.item.item_repository import ItemRepository
from api.domain.student.student_model import Student
from api.domain.student.student_repository import StudentRepository


@pytest.fixture
def ra():
    return 8582


@pytest.fixture
def campus():
    return "AQ"


@pytest.fixture
def modalidade():
    return "EAD"


@pytest.fixture
def data_inicial():
    return "2015-01-01"


@pytest.fixture
def data_fim():
    return "2016-06-11"


@pytest.fixture
def student():
    return Student({
        "nome": "Mock", "idade_ate_31_12_2016": 40, "ra": 12345678, "campus": "CWB",
        "municipio": "Curitiba", "curso": "Medicina", "modalidade": "EAD",
        "nivel_do_curso": ".FIC", "data_inicio": "2015-07-27"
    })


@pytest.fixture
def student_repository():
    return Mock(StudentRepository)


@pytest.fixture
def course_repository():
    return Mock(CourseRepository)


@pytest.fixture
def item_repository():
    return Mock(ItemRepository)




