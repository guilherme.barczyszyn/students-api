from unittest import mock

from api.domain.course.course_repository import CourseRepository


def test_list_courses_by_campus(mongodb, campus):
    with mock.patch.object(CourseRepository, "get_db") as get_db_mock:
        get_db_mock.return_value = mongodb
        repo = CourseRepository()

        courses = repo.list_courses_by_campus(campus)

        assert len(courses) == 2
