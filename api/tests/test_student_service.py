from api.domain.student.student_service import StudentService


def test_find_student(student_repository, ra):
    service = StudentService(student_repository)

    response = service.find(ra)

    assert response.successful


def test_find_student_should_fail(student_repository):
    service = StudentService(student_repository)

    response = service.find(None)

    assert response.successful is False


def test_count_students_registered_between_dates_in_campus(student_repository, campus, data_inicial, data_fim):
    service = StudentService(student_repository)

    response = service.count_registered_between_dates(campus, data_inicial, data_fim)

    assert response.successful


def test_count_students_registered_between_dates_in_campus_should_fail(student_repository, data_inicial, data_fim):
    service = StudentService(student_repository)

    response = service.count_registered_between_dates(None, data_inicial, data_fim)

    assert response.successful is False


def test_insert_student(student_repository, student):
    service = StudentService(student_repository)

    response = service.insert_student(nome=student.nome, idade_ate_31_12_2016=student.idade_ate_31_12_2016,
                                      ra=student.ra, campus=student.campus, modalidade=student.modalidade,
                                      municipio=student.municipio, curso=student.curso, data_inicio=student.data_inicio,
                                      nivel_do_curso=student.nivel_do_curso, cache_student=False)

    assert response.successful


def test_insert_student_should_fail(student_repository, student):
    service = StudentService(student_repository)

    response = service.insert_student(nome=None, idade_ate_31_12_2016=student.idade_ate_31_12_2016,
                                      ra=student.ra, campus=student.campus, modalidade=student.modalidade,
                                      municipio=student.municipio, curso=student.curso, data_inicio=student.data_inicio,
                                      nivel_do_curso=student.nivel_do_curso)

    assert response.successful is False


def test_delete_student(student_repository, ra, campus):
    service = StudentService(student_repository)

    response = service.delete_student(ra, campus)

    assert response.successful


def test_delete_student_should_not_fail(student_repository, ra):
    service = StudentService(student_repository)

    response = service.delete_student(ra, None)

    assert response.successful is False