from unittest import mock

from api.domain.student.student_repository import StudentRepository


def test_find_student(mongodb):
    with mock.patch.object(StudentRepository, "get_db") as get_db_mock:
        get_db_mock.return_value = mongodb
        repo = StudentRepository()
        item = repo.find_by_ra(8582)

    assert item.ra == 8582


def test_get_student_count_should_return_non_zero_value(mongodb, campus, data_inicial, data_fim):
    with mock.patch.object(StudentRepository, "get_db") as get_db_mock:
        get_db_mock.return_value = mongodb
        repo = StudentRepository()

        num = repo.count_registered_between_dates(campus, data_inicial, data_fim)

    assert num == 3


def test_add_new_student(mongodb, student):
    with mock.patch.object(StudentRepository, "get_db") as get_db_mock:
        get_db_mock.return_value = mongodb
        repo = StudentRepository()

        repo.insert_student(student)

        item = repo.find_by_ra(student.ra)

    assert item is not None


def test_remove_student(mongodb, ra, campus):
    with mock.patch.object(StudentRepository, "get_db") as get_db_mock:
        get_db_mock.return_value = mongodb
        repo = StudentRepository()

        repo.delete_student(ra, campus)

        item = repo.find_by_ra(ra)

    assert item is None
