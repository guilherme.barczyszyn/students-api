from unittest import mock

import pymongo

from api.domain.item.item_repository import ItemRepository


def test_list_by_modality_between_dates(mongodb, modalidade, data_inicial, data_fim):
    with mock.patch.object(ItemRepository, "get_db") as get_db_mock:
        mongodb.item.create_index([('data_inicio', pymongo.TEXT)], name='data_inicio_order_index')

        get_db_mock.return_value = mongodb
        repo = ItemRepository()

        items = repo.list_by_modality_between_dates(modalidade=modalidade, data_inicio=data_inicial, data_fim=data_fim)

        assert len(items) > 0
