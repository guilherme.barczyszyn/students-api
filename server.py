from api.domain.course import course_rest
from api.domain.item import item_rest
from api.domain.student import student_rest
from flask import Flask
from api.infra.cache import cache
from flask_swagger_ui import get_swaggerui_blueprint
from yaml import Loader, load

SWAGGER_URL = '/api/docs'
SWAGGER_FILE = 'api/swagger.yaml'

swagger_yml = load(open(SWAGGER_FILE, 'r'), Loader=Loader)

swaggerui_blueprint = get_swaggerui_blueprint(SWAGGER_URL, SWAGGER_FILE, config={'spec': swagger_yml})

app = Flask(__name__)

cache.init_app(app)

app.register_blueprint(student_rest.bp)
app.register_blueprint(course_rest.bp)
app.register_blueprint(item_rest.bp)

app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)

if __name__ == "__main__":
    app.run(debug=True)
