all: test

test:
	python3 -B -m pytest -p no:cacheprovider --cov=./api --cov-report=html

clean:
	rm -rf htmlcov
	rm .coverage