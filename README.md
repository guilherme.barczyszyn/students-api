## Instruções: ##

1. Instale os requisitos contidos no arquivo **requirements.txt**.
2. As variáveis de ambiente estão no arquivo **env_file.py** na raíz do projeto
3. Importação de dados (db_import)
    1. (Opcional) Utilize o docker-compose.yml para subir uma intância do MongoDB via docker. Neste caso o usuário e senha são: mongo. Comando: `docker-compose up`
    2. Para realizar a exportação, basta executar: `python import_data.py`
4. Testes unitários (API).
    1. Para rodar os testes unitários, você pode utilizar o comando `make test`, neste caso será efetuada a verificação de cobertura de testes.Se não desejar verificar a cobertura, execute apenas o pytest no terminal
    2. Se você utilizou o Makefile, Os arquivos de cobertura estarão disponíveis na pasta **htmlcov**. Para limpá-los, execute `make clean`
5. Execução da API
    1. Certifique-se de ter uma instância de MongoDB com os dados importados.
    2. Certifique-se de configurar as variáveis para acesso ao MongoDB, conforme exemplo no arquivo **env_file.py**
    2. Execute o serviço da API: `python server.py`
6. Documentação da API: Swagger disponível em **http://127.0.0.1:5000/api/docs/** com serviço em execução.