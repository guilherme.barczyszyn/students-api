# Script para importar dados csv para collection do MongoDB.
# Este Script realiza essa tarefa com bibliotecas comuns do python como pandas e pymongo.
# Por isso, foi necessário codificar toda a lógica de transformação e importação dos dados.
# Uma solução mais elegante e de poucas linhas de código seria utilizar a ferramenta Spark com
# sua extensão para Python, o PySpark. No entanto, isso exigiria que tivessemos a ferramenta provisionada
# no ambiente de execução.

import json
import math
import os
import pandas
import pymongo
from pymongo import MongoClient

MONGODB_MAX_WRITE_BATCH_SIZE = 100000
MONGODB_HOST = os.getenv('MONGODB_HOST', '127.0.0.1')
MONGODB_USER = os.getenv('MONGODB_USER', 'mongo')
MONGODB_PASSWORD = os.getenv('MONGODB_PASSWORD', 'mongo')
MONGODB_PORT = os.getenv('MONGODB_PORT', '27017')
DATA_FILE = os.getenv('DATA_FILE', 'dataset_estudantes.csv')


def main():
    data_json = read_file()

    # Para os casos em que existem mais de 100k registros:
    # O mongodb possui uma propriedade para evitar estouros chamada
    # MONGODB_MAX_WRITE_BATCH_SIZE que limita o número de inserções
    # simultâneas (insert_many) em no máximo 100k por padrão.
    # Se for o nosso caso, devemos respeitar esse limite inserindo de
    # forma paginada.
    if len(data_json) <= MONGODB_MAX_WRITE_BATCH_SIZE:
        insert_into_mongo(data_json)
    else:
        insert_paginated(data_json)


def read_file():
    data_csv = pandas.read_csv(DATA_FILE)
    return json.loads(data_csv.to_json(orient='records'))


def insert_paginated(data_json):
    data_size = len(data_json)

    page_size = MONGODB_MAX_WRITE_BATCH_SIZE

    total_pages = math.ceil(data_size / page_size)

    idx = 0
    start_idx = 0

    while idx < total_pages:
        data_json_page = split(data_json, start_idx, page_size)

        insert_into_mongo(data_json_page)

        start_idx = start_idx + page_size

        idx += 1


def split(data_json, start_idx, page_size):
    return data_json[start_idx:(start_idx + page_size)]


def insert_into_mongo(json_data):
    client = MongoClient('mongodb://%s:%s@%s:%s' % (MONGODB_USER, MONGODB_PASSWORD, MONGODB_HOST, MONGODB_PORT))
    db = client.students_api

    db.item.insert_many(json_data)


def create_indexes():
    client = MongoClient('mongodb://%s:%s@%s:%s' % (MONGODB_USER, MONGODB_PASSWORD, MONGODB_HOST, MONGODB_PORT))
    db = client.students_api

    db.item.create_index([('ra', pymongo.ASCENDING)], name='search_search_index')
    db.item.create_index([('campus', pymongo.TEXT)], name='campus_search_index')
    db.item.create_index([('data_inicio', pymongo.ASCENDING)], name='data_inicio_order_index')


if __name__ == "__main__":
    main()
